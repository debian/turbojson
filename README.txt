TurboJson
=========

This package provides a template engine plugin, allowing you
to easily use JSON via the simplejson module with TurboGears, Buffet
or other systems that support python.templating.engines.

For information on simplejson, go here:

http://simplejson.github.com/simplejson/

For information on using a template engine plugin with TurboGears
or writing a template engine plugin, go here:

http://www.turbogears.org/1.0/docs/TemplatePlugins
http://www.turbogears.org/1.0/docs/AlternativeTemplating

